﻿/**
 * 最简单的基于FFmpeg的解码器
 * Simplest FFmpeg Decoder
 *
 * 基于雷霄骅Code修改，移植最新的FFmpeg 5.1.2 API
 * 雷神CSDN：http://blog.csdn.net/leixiaohua1020
 *
 * 本程序实现了视频文件的解码(支持HEVC，H.264，MPEG2等)。
 * 是最简单的FFmpeg视频解码方面的教程。
 * 通过学习本例子可以了解FFmpeg的解码流程。
 */

#include <iostream>
extern "C" {
#include <libavutil/imgutils.h>
#include <libavutil/samplefmt.h>
#include <libavutil/timestamp.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
}

static AVFormatContext* fmt_ctx = NULL;
static AVCodecContext *video_dec_ctx = NULL;
static AVStream* video_stream = NULL;
static int video_stream_idx = -1;
static AVFrame *frame = NULL;
static AVPacket *pkt = NULL;
static const char* src_filename = NULL;
FILE* fp_yuv = NULL;
int frame_cnt;

/* 基于当前AVFormatContext，检测AVMediaType对应的流，并打开对应的解码器 */
static int open_codec_context(int *stream_idx,
                              AVCodecContext **dec_ctx, AVFormatContext *fmt_ctx, enum AVMediaType type)
{
    int ret, stream_index;
    AVStream *st;
    const AVCodec *dec = NULL;

 /* 用于找到用户想要寻找的流的信息,执行成功返回要寻找的流的索引 */
    ret = av_find_best_stream(fmt_ctx, type, -1, -1, NULL, 0);
    if (ret < 0) {
        printf("Could not find %s stream in input file '%s'\n",
                av_get_media_type_string(type), src_filename);
        return ret;
    } else {
        stream_index = ret;
        st = fmt_ctx->streams[stream_index];

        /* find decoder for the stream */
        dec = avcodec_find_decoder(st->codecpar->codec_id);
        if (!dec) {
            printf("Failed to find %s codec\n",
                    av_get_media_type_string(type));
            return AVERROR(EINVAL);
        }

        /* Allocate a codec context for the decoder */
        *dec_ctx = avcodec_alloc_context3(dec);
        if (!*dec_ctx) {
            printf("Failed to allocate the %s codec context\n",
                    av_get_media_type_string(type));
            return AVERROR(ENOMEM);
        }

  /* 当前视音频流信息保存在 AVStream中的AVCodecParameters，内存包含w,h，codec_id等 */
  /* AVCodecParameters *codecpar; */
        /* Copy codec parameters from input stream to output codec context */
        if ((ret = avcodec_parameters_to_context(*dec_ctx, st->codecpar)) < 0) {
            printf("Failed to copy %s codec parameters to decoder context\n",
                    av_get_media_type_string(type));
            return ret;
        }
  
  /* 打开编解码器 */
        /* Init the decoders */
        if ((ret = avcodec_open2(*dec_ctx, dec, NULL)) < 0) {
            printf("Failed to open %s codec\n",
                    av_get_media_type_string(type));
            return ret;
        }
        *stream_idx = stream_index;
    }

    return 0;
}

static int decode_packet(AVCodecContext* dec, const AVPacket* pkt)
{
    printf("%s - %d\n", __FUNCTION__, __LINE__);
    int ret = 0;

    // submit the packet to the decoder
    ret = avcodec_send_packet(dec, pkt);
    if (ret < 0) {
        fprintf(stderr, "Error submitting a packet for decoding (%s)\n", av_err2str(ret));
        return ret;
    }

    // get all the available frames from the decoder
    while (ret >= 0) {
        ret = avcodec_receive_frame(dec, frame);
        if (ret < 0) {
            // those two return values are special and mean there is no output
            // frame available, but there were no errors during decoding
            if (ret == AVERROR_EOF || ret == AVERROR(EAGAIN))
                return 0;

            fprintf(stderr, "Error during decoding (%s)\n", av_err2str(ret));
            return ret;
        }

        // write the frame data to output file
        frame_cnt++;
        printf("w= %d，h= %d\n", dec->width, dec->height);
        fwrite(frame->data[0], (dec->width) * (dec->height), 1, fp_yuv);
        fwrite(frame->data[1], 1, dec->width * dec->height/4, fp_yuv);
        fwrite(frame->data[2], 1, dec->width * dec->height/4, fp_yuv);

        /*
        if (dec->codec->type == AVMEDIA_TYPE_VIDEO)
            ret = output_video_frame(frame);
        else
            ret = output_audio_frame(frame);
        */
        av_frame_unref(frame);
        if (ret < 0)
            return ret;
    }
    printf("%s - %d\n", __FUNCTION__, __LINE__);
    return 0;
}

int main(int argc, char** argv)
{
    int ret = 0;
    src_filename = argv[1];

    //avformat_network_init();
    //fmt_ctx = avformat_alloc_context();
    char filepath[] = "Titanic.ts";

    printf("argc = %d,para 1 = %s,para 2 = %s\n",argc,argv[0], filepath);

    printf("%s - %d,file name = %s\n", __FUNCTION__, __LINE__, filepath);
    if (avformat_open_input(&fmt_ctx, filepath, NULL, NULL) < 0)
    { 
        return -1;
    }

    /* retrieve stream information */
    if (avformat_find_stream_info(fmt_ctx, NULL) < 0) {
        printf("Could not find stream information\n");
        return -1;
    }
    /* 新SDK已经不再支持CodecCtx = streams[videoindex]->codec; streams不再含有有效的codeccontext*/
#if 0
    pCodecCtx=pFormatCtx->streams[videoindex]->codec;
    pCodec=avcodec_find_decoder(pCodecCtx->codec_id);
    if(pCodec==NULL)
    {
        printf("Codec not found.\n");
        return -1;
    }
    if(avcodec_open2(pCodecCtx, pCodec,NULL)<0)
    {
        printf("Could not open codec.\n");
        return -1;
    }
#endif
    if (open_codec_context(&video_stream_idx, &video_dec_ctx, fmt_ctx, AVMEDIA_TYPE_VIDEO) >= 0)
    {
        video_stream = fmt_ctx->streams[video_stream_idx];
#if 1//debug
        printf("shichang:%d\n", fmt_ctx->duration);
        printf("name    :%s,iname = %s\n", fmt_ctx->iformat->name, fmt_ctx->iformat->long_name);
#endif
    }
    FILE* fp_h264 = fopen("test264.h264", "wb+");
    fp_yuv = fopen("testyuv.yuv", "wb+");
    if(!video_stream) {
        printf( "Could not find audio or video stream in the input, aborting\n");
        ret = 1;
        goto end;
    }
    frame = av_frame_alloc();
    if (!frame) {
        printf("Could not allocate frame\n");
        ret = AVERROR(ENOMEM);
        goto end;
    }
    /* 为packet分配内存 */
    pkt = av_packet_alloc();
    if (!pkt) {
        printf("Could not allocate packet\n");
        ret = AVERROR(ENOMEM);
        goto end;
    }
    /* 读取音视频包,然后调用解码函数解码 */
    /* read frames from the file */
    while (av_read_frame(fmt_ctx, pkt) >= 0)
    {
        if (pkt->stream_index == video_stream_idx)
        {
            //printf("pac_size = 0X%X,stream_ind = %d\n", pkt->size, pkt->stream_index);
            fwrite(pkt->data, pkt->size, 1, fp_h264);
            ret = decode_packet(video_dec_ctx, pkt);
            printf("frame_cnt = %d, pac_size = 0X%X,stream_ind = %d\n", frame_cnt ,pkt->size, pkt->stream_index);
        }
        av_packet_unref(pkt);
        if (ret < 0)
            break;
    }
    /* flush the decoders */
    if (video_dec_ctx)
        decode_packet(video_dec_ctx, NULL);

    printf("Demuxing succeeded.\n");

end:
    avcodec_free_context(&video_dec_ctx);
    //avcodec_free_context(&audio_dec_ctx);
    avformat_close_input(&fmt_ctx);
    if(fp_h264)
        fclose(fp_h264);
    if(fp_yuv)
        fclose(fp_yuv);
    av_packet_free(&pkt);
    av_frame_free(&frame);

    return ret < 0;
}
