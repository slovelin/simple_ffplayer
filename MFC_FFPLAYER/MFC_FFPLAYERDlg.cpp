﻿
// MFC_FFPLAYERDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "MFC_FFPLAYER.h"
#include "MFC_FFPLAYERDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include <iostream>
extern "C" {
#include <libavutil/imgutils.h>
#include <libavutil/samplefmt.h>
#include <libavutil/timestamp.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <sdl2/SDL.h>
}

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCFFPLAYERDlg 对话框



CMFCFFPLAYERDlg::CMFCFFPLAYERDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFC_FFPLAYER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCFFPLAYERDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT1, m_edit);
}

BEGIN_MESSAGE_MAP(CMFCFFPLAYERDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_PLAY, &CMFCFFPLAYERDlg::OnBnClickedPlay)
	ON_BN_CLICKED(IDC_PAUSE, &CMFCFFPLAYERDlg::OnBnClickedPause)
	ON_BN_CLICKED(IDC_STOP, &CMFCFFPLAYERDlg::OnBnClickedStop)
	ON_BN_CLICKED(IDC_ABOUT, &CMFCFFPLAYERDlg::OnBnClickedAbout)
	ON_BN_CLICKED(IDC_CLOSE, &CMFCFFPLAYERDlg::OnBnClickedClose)
	ON_BN_CLICKED(IDC_FILE, &CMFCFFPLAYERDlg::OnBnClickedFile)
END_MESSAGE_MAP()

static AVFormatContext* fmt_ctx = NULL;
static AVCodecContext* video_dec_ctx = NULL;
static AVStream* video_stream = NULL;
static int video_stream_idx = -1;
static AVFrame* frame = NULL;
static AVPacket* pkt = NULL;
static const char* src_filename = NULL;
int frame_cnt;
int thread_exit = 0;

//Refresh Event
#define SFM_REFRESH_EVENT  (SDL_USEREVENT + 1)
#define SFM_BREAK_EVENT  (SDL_USEREVENT + 2)

int sfp_refresh_thread(void* opaque) {
    thread_exit = 0;
    while (!thread_exit) {
        SDL_Event event;
        event.type = SFM_REFRESH_EVENT;
        SDL_PushEvent(&event);
        SDL_Delay(40);
    }
    thread_exit = 0;
    //Break
    SDL_Event event;
    event.type = SFM_BREAK_EVENT;
    SDL_PushEvent(&event);

    return 0;
}

/* 基于当前AVFormatContext，检测AVMediaType对应的流，并打开对应的解码器 */
static int open_codec_context(int* stream_idx,
    AVCodecContext** dec_ctx, AVFormatContext* fmt_ctx, enum AVMediaType type)
{
    int ret, stream_index;
    AVStream* st;
    const AVCodec* dec = NULL;

    /* 用于找到用户想要寻找的流的信息,执行成功返回要寻找的流的索引 */
    ret = av_find_best_stream(fmt_ctx, type, -1, -1, NULL, 0);
    if (ret < 0) {
        printf("Could not find %s stream in input file '%s'\n",
            av_get_media_type_string(type), src_filename);
        return ret;
    }
    else {
        stream_index = ret;
        st = fmt_ctx->streams[stream_index];

        /* find decoder for the stream */
        dec = avcodec_find_decoder(st->codecpar->codec_id);
        if (!dec) {
            printf("Failed to find %s codec\n",
                av_get_media_type_string(type));
            return AVERROR(EINVAL);
        }

        /* Allocate a codec context for the decoder */
        *dec_ctx = avcodec_alloc_context3(dec);
        if (!*dec_ctx) {
            printf("Failed to allocate the %s codec context\n",
                av_get_media_type_string(type));
            return AVERROR(ENOMEM);
        }

        /* 当前视音频流信息保存在 AVStream中的AVCodecParameters，内存包含w,h，codec_id等 */
        /* AVCodecParameters *codecpar; */
              /* Copy codec parameters from input stream to output codec context */
        if ((ret = avcodec_parameters_to_context(*dec_ctx, st->codecpar)) < 0) {
            printf("Failed to copy %s codec parameters to decoder context\n",
                av_get_media_type_string(type));
            return ret;
        }

        /* 打开编解码器 */
              /* Init the decoders */
        if ((ret = avcodec_open2(*dec_ctx, dec, NULL)) < 0) {
            printf("Failed to open %s codec\n",
                av_get_media_type_string(type));
            return ret;
        }
        *stream_idx = stream_index;
    }

    return 0;
}

UINT ffmpeg_player(LPVOID lpPara)
{
    int ret = 0;

    //SDL
    int screen_w, screen_h;
    SDL_Window* screen;
    SDL_Renderer* sdlRenderer;
    SDL_Texture* sdlTexture;
    SDL_Rect sdlRect;
    SDL_Thread* video_tid;
    SDL_Event event;

    CMFCFFPLAYERDlg* dlg = (CMFCFFPLAYERDlg*)lpPara;
    char filepath[500] = { 0 };
    GetWindowTextA(dlg->m_edit, (LPSTR)filepath, 500);

    if (avformat_open_input(&fmt_ctx, filepath, NULL, NULL) < 0)
    {
        return -1;
    }

    /* retrieve stream information */
    if (avformat_find_stream_info(fmt_ctx, NULL) < 0) {
        printf("Could not find stream information\n");
        return -1;
    }
    /* 新SDK已经不再支持CodecCtx = streams[videoindex]->codec; streams不再含有有效的codeccontext*/
#if 0
    pCodecCtx = pFormatCtx->streams[videoindex]->codec;
    pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
    if (pCodec == NULL)
    {
        printf("Codec not found.\n");
        return -1;
    }
    if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0)
    {
        printf("Could not open codec.\n");
        return -1;
    }
#endif
    if (open_codec_context(&video_stream_idx, &video_dec_ctx, fmt_ctx, AVMEDIA_TYPE_VIDEO) >= 0)
    {
        video_stream = fmt_ctx->streams[video_stream_idx];
#if 1//debug
        printf("shichang:%d\n", fmt_ctx->duration);
        printf("name    :%s,iname = %s\n", fmt_ctx->iformat->name, fmt_ctx->iformat->long_name);
        printf("kuan gao :%d,%d\n", video_dec_ctx->width, video_dec_ctx->height);
#endif
    }
    if (!video_stream) {
        printf("Could not find audio or video stream in the input, aborting\n");
        ret = 1;
        goto end;
    }
    frame = av_frame_alloc();
    if (!frame) {
        printf("Could not allocate frame\n");
        ret = AVERROR(ENOMEM);
        goto end;
    }
    /* 为packet分配内存 */
    pkt = av_packet_alloc();
    if (!pkt) {
        printf("Could not allocate packet\n");
        ret = AVERROR(ENOMEM);
        goto end;
    }

    if (SDL_Init(SDL_INIT_VIDEO)) {
        printf("Could not initialize SDL - %s\n", SDL_GetError());
        return -1;
    }
    screen_w = video_dec_ctx->width;
    screen_h = video_dec_ctx->height;

    //显示在弹出窗口
    screen = SDL_CreateWindow("Simplest Video Play SDL2", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
       screen_w, screen_h, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    //===========================================
    //显示在MFC控件上
    screen = SDL_CreateWindowFrom((void*)(dlg->GetDlgItem(IDC_SCREEN)->GetSafeHwnd()));
    if (!screen) {
        AfxMessageBox(__T("SDL: could not create window - exiting\n"));
        return -1;
    }
    sdlRenderer = SDL_CreateRenderer(screen, -1, SDL_RENDERER_PRESENTVSYNC);
    sdlTexture = SDL_CreateTexture(sdlRenderer, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, video_dec_ctx->width, video_dec_ctx->height);
    video_tid = SDL_CreateThread(sfp_refresh_thread, NULL, NULL);
    for (;;)
    {
        SDL_WaitEvent(&event);// 从事件队列中取事件
        if (event.type == SFM_REFRESH_EVENT)
        {
            while (1) {
                if (av_read_frame(fmt_ctx, pkt) < 0) {
                    thread_exit = 1;
                }
                if (pkt->stream_index == video_stream_idx) {
                    break;
                }
            }
            avcodec_send_packet(video_dec_ctx, pkt);
            while (avcodec_receive_frame(video_dec_ctx, frame) == 0) {
                //SDL 刷新纹理
                SDL_UpdateYUVTexture(sdlTexture, NULL, frame->data[0], frame->linesize[0],
                    frame->data[1], frame->linesize[1],
                    frame->data[2], frame->linesize[2]);

                sdlRect.x = 0;
                sdlRect.y = 0;
                sdlRect.w = screen_w;
                sdlRect.h = screen_h;

                SDL_RenderClear(sdlRenderer);
                SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, &sdlRect);
                SDL_RenderPresent(sdlRenderer);
            }
            av_packet_unref(pkt);
        }
        else if (event.type == SDL_WINDOWEVENT) {
            SDL_GetWindowSize(screen, &screen_w, &screen_h);
        }
        else if (event.type == SDL_QUIT) {
            thread_exit = 1;
        }
        else if (event.type == SFM_BREAK_EVENT) {
            break;
        }
    }

    printf("Demuxing succeeded.\n");

    SDL_DestroyWindow(screen);
    SDL_Quit();
    //FIX Small Bug
    //SDL Hide Window When it finished
    dlg->GetDlgItem(IDC_SCREEN)->ShowWindow(SW_SHOWNORMAL);

end:
    avcodec_free_context(&video_dec_ctx);
    avformat_close_input(&fmt_ctx);
    av_packet_free(&pkt);
    av_frame_free(&frame);

    return ret < 0;
}

// CMFCPlayerDlg 消息处理程序

BOOL CMFCFFPLAYERDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFCFFPLAYERDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFCFFPLAYERDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFCFFPLAYERDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCFFPLAYERDlg::OnBnClickedPlay()
{
    AfxBeginThread(ffmpeg_player,this);
}


void CMFCFFPLAYERDlg::OnBnClickedPause()
{
	// TODO: 在此添加控件通知处理程序代码
}


void CMFCFFPLAYERDlg::OnBnClickedStop()
{
	CAboutDlg dlgl;
	dlgl.DoModal();
	// TODO: 在此添加控件通知处理程序代码
}


void CMFCFFPLAYERDlg::OnBnClickedAbout()
{
	// TODO: 在此添加控件通知处理程序代码
}


void CMFCFFPLAYERDlg::OnBnClickedClose()
{
	// TODO: 在此添加控件通知处理程序代码
}


void CMFCFFPLAYERDlg::OnBnClickedFile()
{
	CString FilePathName;
	CFileDialog dlg(TRUE, NULL, NULL, NULL, NULL);
	if (dlg.DoModal() == IDOK) {
		FilePathName = dlg.GetPathName();
		m_edit.SetWindowText(FilePathName);
	}
	// TODO: 在此添加控件通知处理程序代码
}
