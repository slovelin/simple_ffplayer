﻿/**
 * 最简单的基于FFmpeg的解码器
 * Simplest FFmpeg Decoder
 *
 * 基于雷霄骅Code修改，移植最新的FFmpeg 5.1.2 API
 * 雷神CSDN：http://blog.csdn.net/leixiaohua1020
 *
 * 本程序实现了视频文件的解码(支持HEVC，H.264，MPEG2等)。
 * 是最简单的FFmpeg视频解码方面的教程。
 * 通过学习本例子可以了解FFmpeg的解码流程。
 */

#include <iostream>
extern "C" {
#include <libavutil/imgutils.h>
#include <libavutil/samplefmt.h>
#include <libavutil/timestamp.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <sdl2/SDL.h>
}

static AVFormatContext* fmt_ctx = NULL;
static AVCodecContext *video_dec_ctx = NULL;
static AVStream* video_stream = NULL;
static int video_stream_idx = -1;
static AVFrame *frame = NULL;
static AVPacket *pkt = NULL;
static const char* src_filename = NULL;
int frame_cnt;

//Refresh Event
#define SFM_REFRESH_EVENT  (SDL_USEREVENT + 1)

#define SFM_BREAK_EVENT  (SDL_USEREVENT + 2)

int thread_exit=0;

int sfp_refresh_thread(void *opaque){
	thread_exit=0;
	while (!thread_exit) {
		SDL_Event event;
		event.type = SFM_REFRESH_EVENT;
		SDL_PushEvent(&event);
		SDL_Delay(40);
	}
	thread_exit=0;
	//Break
	SDL_Event event;
	event.type = SFM_BREAK_EVENT;
	SDL_PushEvent(&event);
    return 0;
}

/* 基于当前AVFormatContext，检测AVMediaType对应的流，并打开对应的解码器 */
static int open_codec_context(int *stream_idx,
                              AVCodecContext **dec_ctx, AVFormatContext *fmt_ctx, enum AVMediaType type)
{
    int ret, stream_index;
    AVStream *st;
    const AVCodec *dec = NULL;

 /* 用于找到用户想要寻找的流的信息,执行成功返回要寻找的流的索引 */
    ret = av_find_best_stream(fmt_ctx, type, -1, -1, NULL, 0);
    if (ret < 0) {
        printf("Could not find %s stream in input file '%s'\n",
                av_get_media_type_string(type), src_filename);
        return ret;
    } else {
        stream_index = ret;
        st = fmt_ctx->streams[stream_index];

        /* find decoder for the stream */
        dec = avcodec_find_decoder(st->codecpar->codec_id);
        if (!dec) {
            printf("Failed to find %s codec\n",
                    av_get_media_type_string(type));
            return AVERROR(EINVAL);
        }

        /* Allocate a codec context for the decoder */
        *dec_ctx = avcodec_alloc_context3(dec);
        if (!*dec_ctx) {
            printf("Failed to allocate the %s codec context\n",
                    av_get_media_type_string(type));
            return AVERROR(ENOMEM);
        }

  /* 当前视音频流信息保存在 AVStream中的AVCodecParameters，内存包含w,h，codec_id等 */
  /* AVCodecParameters *codecpar; */
        /* Copy codec parameters from input stream to output codec context */
        if ((ret = avcodec_parameters_to_context(*dec_ctx, st->codecpar)) < 0) {
            printf("Failed to copy %s codec parameters to decoder context\n",
                    av_get_media_type_string(type));
            return ret;
        }
  
  /* 打开编解码器 */
        /* Init the decoders */
        if ((ret = avcodec_open2(*dec_ctx, dec, NULL)) < 0) {
            printf("Failed to open %s codec\n",
                    av_get_media_type_string(type));
            return ret;
        }
        *stream_idx = stream_index;
    }

    return 0;
}

static int decode_packet(AVCodecContext* dec, const AVPacket* pkt)
{
    printf("%s - %d\n", __FUNCTION__, __LINE__);
    int ret = 0;

    // submit the packet to the decoder
    ret = avcodec_send_packet(dec, pkt);
    if (ret < 0) {
        fprintf(stderr, "Error submitting a packet for decoding (%s)\n", av_err2str(ret));
        return ret;
    }

    // get all the available frames from the decoder
    while (ret >= 0) {
        ret = avcodec_receive_frame(dec, frame);
        if (ret < 0) {
            // those two return values are special and mean there is no output
            // frame available, but there were no errors during decoding
            if (ret == AVERROR_EOF || ret == AVERROR(EAGAIN))
                return 0;

            fprintf(stderr, "Error during decoding (%s)\n", av_err2str(ret));
            return ret;
        }

        // write the frame data to output file
        frame_cnt++;
        printf("w= %d，h= %d\n", dec->width, dec->height);

        /*
        if (dec->codec->type == AVMEDIA_TYPE_VIDEO)
            ret = output_video_frame(frame);
        else
            ret = output_audio_frame(frame);
        */
        av_frame_unref(frame);
        if (ret < 0)
            return ret;
    }
    printf("%s - %d\n", __FUNCTION__, __LINE__);
    return 0;
}

int main(int argc, char** argv)
{
    int ret = 0;
    src_filename = argv[1];

    //SDL
    int screen_w, screen_h;
    SDL_Window* screen;
    SDL_Renderer* sdlrenderer;
    SDL_Texture* sdltexture;
    SDL_Rect sdlRect;
    SDL_Thread* video_tid;
    SDL_Event event;

    char filepath[] = "test.mov";
    printf("argc = %d,para 1 = %s,para 2 = %s\n",argc,argv[0], filepath);

    printf("%s - %d,file name = %s\n", __FUNCTION__, __LINE__, filepath);
    if (avformat_open_input(&fmt_ctx, filepath, NULL, NULL) < 0)
    { 
        return -1;
    }

    /* retrieve stream information */
    if (avformat_find_stream_info(fmt_ctx, NULL) < 0) {
        printf("Could not find stream information\n");
        return -1;
    }
    /* 新SDK已经不再支持CodecCtx = streams[videoindex]->codec; streams不再含有有效的codeccontext*/
#if 0
    pCodecCtx=pFormatCtx->streams[videoindex]->codec;
    pCodec=avcodec_find_decoder(pCodecCtx->codec_id);
    if(pCodec==NULL)
    {
        printf("Codec not found.\n");
        return -1;
    }
    if(avcodec_open2(pCodecCtx, pCodec,NULL)<0)
    {
        printf("Could not open codec.\n");
        return -1;
    }
#endif
    if (open_codec_context(&video_stream_idx, &video_dec_ctx, fmt_ctx, AVMEDIA_TYPE_VIDEO) >= 0)
    {
        video_stream = fmt_ctx->streams[video_stream_idx];
#if 1//debug
        printf("shichang:%d\n", fmt_ctx->duration);
        printf("name    :%s,iname = %s\n", fmt_ctx->iformat->name, fmt_ctx->iformat->long_name);
#endif
    }

    if(!video_stream) {
        printf( "Could not find audio or video stream in the input, aborting\n");
        ret = 1;
        goto end;
    }
    frame = av_frame_alloc();
    if (!frame) {
        printf("Could not allocate frame\n");
        ret = AVERROR(ENOMEM);
        goto end;
    }
    /* 为packet分配内存 */
    pkt = av_packet_alloc();
    if (!pkt) {
        printf("Could not allocate packet\n");
        ret = AVERROR(ENOMEM);
        goto end;
    }
	

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER)) {  
        printf("Could not initialize SDL - %s\n", SDL_GetError());
        return -1;
    }
    //SDL 2.0 Support for multiple windows
    screen_w = video_dec_ctx->width;
    screen_h = video_dec_ctx->height;
    screen = SDL_CreateWindow("Play SDL2", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        screen_w, screen_h, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    if (!screen) {
        printf("SDL: could not create window - exiting:%s\n", SDL_GetError());
        return -1;
    }
    sdlrenderer = SDL_CreateRenderer(screen, -1, 0);
    sdltexture = SDL_CreateTexture(sdlrenderer, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, video_dec_ctx->width, video_dec_ctx->height);

	sdlRect.x=0;
	sdlRect.y=0;
	sdlRect.w=screen_w;
	sdlRect.h=screen_h;

	video_tid = SDL_CreateThread(sfp_refresh_thread,NULL,NULL);
	//------------SDL End------------
	//Event Loop
	
	for (;;) {
		//Wait
		SDL_WaitEvent(&event);
		if(event.type==SFM_REFRESH_EVENT){
			if(av_read_frame(fmt_ctx, pkt) >= 0){
        		if (pkt->stream_index == video_stream_idx){
					ret = decode_packet(video_dec_ctx, pkt);
            		if (ret < 0)
					{
						printf("Decode Error.\n");
						return -1;
					}
					else
            		{
						//SDL---------------------------
						SDL_UpdateTexture(sdltexture, NULL, frame->data[0], frame->linesize[0]);

	                    SDL_RenderClear(sdlrenderer);
	                    SDL_RenderCopy(sdlrenderer, sdltexture, NULL, NULL);
	                    SDL_RenderPresent(sdlrenderer);
						//SDL End-----------------------
					}
				}
			}
		}else if (event.type == SDL_WINDOWEVENT) {
			//If Resize
			SDL_GetWindowSize(screen, &screen_w, &screen_h);
		}else if (event.type == SDL_QUIT) {
			thread_exit = 1;
		}else if(event.type==SFM_BREAK_EVENT){
                    break;
        }
            
        printf("frame_cnt = %d, pac_size = 0X%X,stream_ind = %d\n", frame_cnt ,pkt->size, pkt->stream_index);
        av_packet_unref(pkt);
        if (ret < 0)
            break;
    }
    /* flush the decoders */
    if (video_dec_ctx)
        decode_packet(video_dec_ctx, NULL);

    printf("Demuxing succeeded.\n");

end:
    avcodec_free_context(&video_dec_ctx);
    //avcodec_free_context(&audio_dec_ctx);
    avformat_close_input(&fmt_ctx);
    av_packet_free(&pkt);
    av_frame_free(&frame);

    return ret < 0;
}
