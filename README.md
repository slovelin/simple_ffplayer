# Simple_FFplayer

#### 介绍
基于FFmpeg及SDL2实现一个简易播放器。项目在雷神-雷霄骅的开源项目基础上，更新了最新的FFmpeg 5.1.2库及对应API接口重新实现。
雷神的开源项目是15年写的，如今FFmpeg版本已经发生了很大的变化,当初代码中的不少API已经无法使用。所以在雷神原来项目的基础上，结合最新的FFmpeg和SDL2，重新实现了播放器。

#### 软件架构
思路仍然是雷神那种各个模块逐步实现/层层递进的方式，最后再组合实现一个简单的播放器。其中：
- 1.Decoding 实现一个视频文件的Demux+解码的过程。过程中可以保存Demux后的H264文件，Decode后的YUV文件。
- 2.SDL2_Video_Show 通过SDL2进行YUV文件播放。
- 3.SDL2_Video_Show_EX在上一个项目基础上，增加了SDL事件和SDL对线程，在播放时可以处理其他事件。
- 4.FFplayer 实现了一个完整的弹出框的播放器，即集成了前面1-3的功能：编码的视频文件->YUV->SDL2。
- 5.MFC_FFPLAYER通过MFC，在项目4基础上实现一个带界面的视频播放器。

#### 使用说明

1.  其实分项目5，即MFC还有一点小问题：
```
	//1.正常创建Window方法，但无法出图
	screen = SDL_CreateWindowFrom((void*)(dlg->GetDlgItem(IDC_SCREEN)->GetSafeHwnd()));

	//2.不正常的创建window方法，但可以出图。使用MFC时应该用SDL_CreateWindowFrom。
	//显示在弹出窗口
	screen = SDL_CreateWindow("Simplest Video Play SDL2", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,screen_w, screen_h, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	//===========================================
	/显示在MFC控件上
	screen = SDL_CreateWindowFrom((void*)(dlg->GetDlgItem(IDC_SCREEN)->GetSafeHwnd()));
	
	//刚换工作，需在最短的时间，尽可能掌握工作直接相关的知识，后续再深入研究了。
```
	
2.  一个建议：在Window系统使用FFmpeg时，初学建议直接用官网编译好的的库，不要自己编译！！！能不能编译过，编译过能不能用，涉及到太多方面：你的FFmpeg版本，你安装的MSYS2版本，你的MSYS2上
	安装的各类编译工具的版本，你的VS的版本，，，，坑很多，可能你花费一天时间都不能成功！如果你是在Linux上编译就很方便，我下源码后，只花了2分钟不到就编译成功了，且能正常使用。

#### 参与贡献
1. 致敬雷霄骅！
