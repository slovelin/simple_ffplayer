﻿/**
 * 最简单的SDL2播放视频的例子（SDL2播放RGB/YUV）
 * Simplest Video Play SDL2 (SDL2 play RGB/YUV)
 *
 * 雷霄骅 Lei Xiaohua
 * leixiaohua1020@126.com
 * 中国传媒大学/数字电视技术
 * Communication University of China / Digital TV Technology
 * http://blog.csdn.net/leixiaohua1020
 *
 * 本程序使用SDL2播放RGB/YUV视频像素数据。SDL实际上是对底层绘图
 * API（Direct3D，OpenGL）的封装，使用起来明显简单于直接调用底层
 * API。
 *
 * This software plays RGB/YUV raw video data using SDL2.
 * SDL is a wrapper of low-level API (Direct3D, OpenGL).
 * Use SDL is much easier than directly call these low-level API.
 */

#include <stdio.h>

extern "C"
{
#include "SDL.h"
}

const int bpp = 12;

int screen_w = 640, screen_h = 360;
const int pixel_w = 640, pixel_h = 360;

unsigned char buffer[pixel_w * pixel_h * bpp / 8];

int main(int argc, char* argv[])
{
	//1.1 初始化SDL系统
	if (SDL_Init(SDL_INIT_VIDEO)) {
		printf("Could not initialize SDL - %s\n", SDL_GetError());
		return -1;
	}

	SDL_Window* screen;
	//1.2 创建窗口SDL_Window
	screen = SDL_CreateWindow("Simplest Video Play SDL2", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		screen_w, screen_h, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	if (!screen) {
		printf("SDL: could not create window - exiting:%s\n", SDL_GetError());
		return -1;
	}
	//1.3 创建渲染器SDL_CreateRenderer
	SDL_Renderer* sdlRenderer = SDL_CreateRenderer(screen, -1, 0);

	Uint32 pixformat = 0;
	//IYUV: Y + U + V  (3 planes)
	//YV12: Y + V + U  (3 planes)
	pixformat = SDL_PIXELFORMAT_IYUV;

	// 1.4 创建纹理SDL_Texture
	SDL_Texture* sdlTexture = SDL_CreateTexture(sdlRenderer, pixformat, SDL_TEXTUREACCESS_STREAMING, pixel_w, pixel_h);

	// 2.0 打开准备显示的YUV文件
	FILE* fp = NULL;
	fp = fopen("sintel_640_360.yuv", "rb+");

	if (fp == NULL) {
		printf("cannot open this file\n");
		return -1;
	}

	// 2.1 创建矩形解构：用于多画面显示
	SDL_Rect sdlRect;

	while (1) {
		// 2.2 循环读出每帧画面的数据
		if (fread(buffer, 1, pixel_w * pixel_h * bpp / 8, fp) != pixel_w * pixel_h * bpp / 8) {
			// Loop - 播完一遍后重新从头开始读
			fseek(fp, 0, SEEK_SET);
			fread(buffer, 1, pixel_w * pixel_h * bpp / 8, fp);
		}  

		// 2.3 设置纹理的数据
		SDL_UpdateTexture(sdlTexture, NULL, buffer, pixel_w);

		sdlRect.x = 0;
		sdlRect.y = 0;
		sdlRect.w = screen_w;
		sdlRect.h = screen_h;

		// 2.4 清除渲染器中上一帧的数据
		SDL_RenderClear(sdlRenderer);
		// 2.5 将纹理的数据拷贝给渲染器
		SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, &sdlRect);
		// 2.6 显示
		SDL_RenderPresent(sdlRenderer);
		//Delay 40ms
		SDL_Delay(40);

	}
	// 3.0 退出SDL系统
	SDL_Quit();
	return 0;
}
